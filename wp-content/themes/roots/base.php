<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

   <?php /* Google Tag Manager */ ?>
   <?php if( get_field('google_tag_manager_body', 'option') ): ?>
      <?php the_field('google_tag_manager_body', 'option'); ?>
   <?php endif; ?>  

    <!--[if lt IE 8]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
      </div>
    <![endif]-->

  <?php
    do_action('get_header');
    // Choose the correct header
    // pick which one in config.php
    if (current_theme_supports('header-two-navs')) {
      get_template_part( 'templates/header-two-navs' );
    } elseif (current_theme_supports('header-nav-bottom')) {
      get_template_part( 'templates/header-nav-bottom' );
    } else {
      get_template_part( 'templates/header' );
    }
  ?>

    <?php if(!is_front_page() ): ?>
        <div class="page-header-wrap">
            <div class="container">
                <h1><?php echo roots_title(); ?></h1>
            </div>
        </div>
    <?php endif; ?>


    <div class="site-main wrap" role="document">
        <div class="content row">
          <main class="main <?php echo roots_main_class(); ?>" role="main">
            <?php include roots_template_path(); ?>
          </main><!-- /.main -->
          <?php if (roots_display_sidebar()) : ?>
            <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
              <?php include roots_sidebar_path(); ?>
            </aside><!-- /.sidebar -->
          <?php endif; ?>
        </div><!-- /.content -->
    </div><!-- /.wrap -->

    <?php if (!is_front_page() || !is_page(25) ): ?>
        <?php if ( is_active_sidebar( 'postscript-a' ) ) { ?>
            <div class="postscript-a">
                <?php dynamic_sidebar( 'postscript-a' ); ?>
            </div>
        <?php } ?>
    <?php endif; ?>


  <?php get_template_part('templates/footer'); ?>

</body>
</html>
